import pandas as pd

# Read the CSV file
data = pd.read_csv('nutrition_modified.csv')

# Drop duplicates based on the 'name' column, keeping the first occurrence
filtered_data = data.drop_duplicates(subset='name', keep='first')

# Save the filtered data to a new CSV file
filtered_data.to_csv('nutrition_modified_filter.csv', index=False)

