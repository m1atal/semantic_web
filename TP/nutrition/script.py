import pandas as pd

# Charger le fichier CSV
df = pd.read_csv("/TP/nutrition/nutrition.csv")

# Extraire le premier mot de la colonne "name"
df['name'] = df['name'].str.split(',').str[0]

# Afficher les premières lignes du DataFrame résultant
print(df.head())

# Enregistrer le DataFrame modifié dans un nouveau fichier CSV
df.to_csv("nutrition_modified.csv", index=False)